# Tinyurl

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Tinyurl is a java project using play framework. In order to run the service, use the below command.
  - sbt run

# New Features!

  - Creating tinyurl - http://localhost:9000/tinyurl?url=www.google.com
  - Opening tinyurl - http://localhost:9000/open?tinyurl=1000