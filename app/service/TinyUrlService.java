package service;

public class TinyUrlService  {
    private static int series = 999;
    DataProvider dp = null;

  public TinyUrlService() {
      dp = new DataProvider();
  }

    public String generateTinyUrl(String url) {
        String tinyurl = tinyUrlAlgorithm(url);
        dp.insertUrl(tinyurl, url);
        return tinyurl;
    }

    public String retrieveUrl(String tinyUrl) {
        return  dp.retrieveUrl(tinyUrl);
    }

    private String tinyUrlAlgorithm(String url) {
        series++;
        return String.valueOf(series);
    }
}
