// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/ecom-venkatesh.k1/code/tinyurl/conf/routes
// @DATE:Sun Sep 01 23:37:42 IST 2019

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:6
package controllers.javascript {

  // @LINE:6
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def openTinyUrl: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.openTinyUrl",
      """
        function(tinyurl0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "open" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("tinyurl", tinyurl0)])})
        }
      """
    )
  
    // @LINE:7
    def computeTinyUrl: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.computeTinyUrl",
      """
        function(url0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "tinyurl" + _qS([(""" + implicitly[play.api.mvc.QueryStringBindable[String]].javascriptUnbind + """)("url", url0)])})
        }
      """
    )
  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }

  // @LINE:11
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:11
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }


}
