// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/ecom-venkatesh.k1/code/tinyurl/conf/routes
// @DATE:Sun Sep 01 23:37:42 IST 2019


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
