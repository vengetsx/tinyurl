package service;

import java.util.HashMap;
import java.util.Map;

public class DataProvider {

    Map<String, String> list = new HashMap<String, String>();

    public void insertUrl(String tinyUrl, String url) {
        list.put( tinyUrl, url);
    }

    public String retrieveUrl(String tinyUrl) {
        return list.get(tinyUrl);
    }
}
