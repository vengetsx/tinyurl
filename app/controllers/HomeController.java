package controllers;

import play.mvc.*;
import service.TinyUrlService;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    TinyUrlService service;
    HomeController() {
        service = new TinyUrlService();
    }

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
//    public Result index() {
//        return ok(views.html.index.render());
//    }

    public Result index() {
        return ok(views.html.home.render());
    }

    public Result computeTinyUrl(String url){
        return ok(service.generateTinyUrl(url));
    }

    public Result openTinyUrl(String tinyurl) {
        return redirect("http://" + service.retrieveUrl(tinyurl));
    }
}
